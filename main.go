package main

import "fmt"

func main() {
	fmt.Printf("Hello, world.\n")

	var a int = 1
	var b int = 2
	var c int = Add(a, b)
	fmt.Printf("%d + %d = %d\n", a, b, c)
}

func Add(x int, y int) int {
	return x + y
}
